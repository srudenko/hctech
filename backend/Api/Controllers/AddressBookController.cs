﻿using System.Net;
using System.Threading.Tasks;
using Contracts.Commands;
using Contracts.Exceptions;
using Contracts.Queries;
using Contracts.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AddressBookController: ControllerBase
    {
        private readonly IMediator _mediator;

        public AddressBookController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("all")]
        [SwaggerOperation(Summary = "Returns all available contacts in the system")]
        [SwaggerResponse((int) HttpStatusCode.OK, "Operation completed successfully", typeof(ListContactsQueryResponse))]
        [SwaggerResponse((int) HttpStatusCode.BadRequest, "Invalid model provided")]
        public async Task<IActionResult> List([FromQuery] ListContactsQuery query)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var response = await _mediator.Send(query);
            return Ok(response);
        }

        // Exposing emails in query string is a bad practice
        // ideally this method should use Id instead of email
        [HttpGet()]
        [SwaggerOperation(Summary = "Returns a contact by internal id")]
        [SwaggerResponse((int) HttpStatusCode.OK, "Operation completed successfully", typeof(ContactQueryResponse))]
        [SwaggerResponse((int) HttpStatusCode.BadRequest, "Invalid model provided")]

        public async Task<IActionResult> Get([FromQuery] GetContactByEmailQuery query)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var response = await _mediator.Send(query);

            if (response == null)
            {
                return NotFound();
            }

            return Ok(response);
        }

        [HttpPost()]
        [SwaggerOperation(Summary = "Creates a new contact")]
        [SwaggerResponse((int) HttpStatusCode.OK, "Operation completed successfully", typeof(ContactQueryResponse))]
        [SwaggerResponse((int) HttpStatusCode.Conflict, "The user already exist")]
        [SwaggerResponse((int) HttpStatusCode.BadRequest, "Invalid model provided")]
        public async Task<IActionResult> Create([FromBody] CreateContactCommand command)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                var response = await _mediator.Send(command);

                return Ok(response);
            }
            catch (UserExistException)
            {
                return Conflict("User already exist");
            }
        }

        [HttpPut()]
        [SwaggerOperation(Summary = "Updates the existing contact")]
        [SwaggerResponse((int) HttpStatusCode.Accepted, "Operation completed successfully", typeof(ContactQueryResponse))]
        [SwaggerResponse((int) HttpStatusCode.NotFound, "The user not found")]
        [SwaggerResponse((int) HttpStatusCode.BadRequest, "Invalid model provided")]
        public async Task<IActionResult> Update([FromBody] UpdateContactCommand command)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                var response = await _mediator.Send(command);

                return Accepted(response);
            }
            catch (NotFoundException)
            {
                return NotFound("User not found");
            }
        }
    }
}