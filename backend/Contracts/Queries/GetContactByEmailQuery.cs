﻿using System.ComponentModel.DataAnnotations;
using Contracts.Responses;
using MediatR;

namespace Contracts.Queries
{
    public record GetContactByEmailQuery: IRequest<ContactQueryResponse>
    {
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }
    };
}