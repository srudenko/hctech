﻿using Contracts.Responses;
using MediatR;

namespace Contracts.Queries
{
    public record ListContactsQuery: IRequest<ListContactsQueryResponse>;
}