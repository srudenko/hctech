﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts.Models
{
    public record ContactBase
    {
        /// <summary>
        /// FirstName
        /// <example>Mike</example>
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }

        /// <summary>
        /// <example>Dow</example>
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Surname { get; set; }

        /// <summary>
        /// <example>2010-01-01</example>
        /// </summary>
        [Required]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// mike.dow@test.com
        /// </summary>
        [Required]
        [MaxLength(256)]
        [EmailAddress]
        public string EmailAddress { get; set; }
    }
}