﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Contracts.Models
{
    public record ContactUpdate: ContactBase, IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DateOfBirth.Date >= DateTime.UtcNow.Date)
            {
                yield return new ValidationResult("Date of Birth must be in the past");
            }

            if (DateOfBirth.Date != DateOfBirth)
            {
                yield return new ValidationResult("Date of Birth must not include time part");
            }
        }
    }
}