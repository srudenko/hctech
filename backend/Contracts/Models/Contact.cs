using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts.Models
{
    public record Contact : ContactBase
    {
        [Required]
        public Guid Id { get; set; }
    }
}