﻿using Contracts.Models;
using Contracts.Responses;
using MediatR;

namespace Contracts.Commands
{
    public record CreateContactCommand : ContactUpdate, IRequest<ContactQueryResponse>
    {
    }
}