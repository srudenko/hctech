﻿using Contracts.Models;
using Contracts.Responses;
using MediatR;

namespace Contracts.Commands
{
    public record UpdateContactCommand: ContactUpdate, IRequest<ContactQueryResponse>;
}