﻿using Contracts.Models;

namespace Contracts.Responses
{
    public record ContactQueryResponse: Contact;
}