﻿using System.Collections.Generic;
using Contracts.Models;

namespace Contracts.Responses
{
    public record ListContactsQueryResponse
    {
        public ListContactsQueryResponse(IList<Contact> contacts)
        {
            Contacts = contacts;
        }

        public IList<Contact> Contacts { get; }
    };
}