using System;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;
using Api.Tests.Helpers;
using Contracts.Commands;
using Contracts.Responses;
using Moq;
using Web;
using Xunit;

namespace Api.Tests
{
    public class AddressBookControllerTests : IClassFixture<WebServerRunner<Startup>>
    {
        private readonly WebServerRunner<Startup> _webServer;

        public AddressBookControllerTests(WebServerRunner<Startup> webServer)
        {
            _webServer = webServer;

            _webServer.MicroserviceHubMock.Invocations.Clear();
        }

        [Fact]
        public async Task Create_Should_validate_date_of_birth_in_past()
        {
            // arrange
            using var httpClient = _webServer.TestServer.CreateClient();

            var request = new CreateContactCommand
            {
                Surname = "User",
                FirstName = "One",
                EmailAddress = "user.one@test.com",
                DateOfBirth = DateTime.Today.AddDays(3)
            };

            // act

            var response = await httpClient.PostAsJsonAsync($"/AddressBook", request);


            // assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

            var validationModel = await response.Content.ReadAsJson<ModelStateError>();

            Assert.Contains("Date of Birth must be in the past", validationModel.Errors[""][0]);

        }

        [Fact]
        public async Task Update_Should_validate_date_of_birth_is_a_date()
        {
            // arrange
            using var httpClient = _webServer.TestServer.CreateClient();

            var request = new UpdateContactCommand
            {
                Surname = "User",
                FirstName = "One",
                EmailAddress = "user.one@test.com",
                DateOfBirth = DateTime.UtcNow.AddDays(-1)
            };

            // act

            var response = await httpClient.PutAsJsonAsync("/AddressBook", request);


            // assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

            var validationModel = await response.Content.ReadAsJson<ModelStateError>();

            Assert.Contains("Date of Birth must not include time part", validationModel.Errors[""][0]);

        }

        [Fact]
        public async Task Create_Should_validate_all_fields_ara_mandatory()
        {
            // arrange
            using var httpClient = _webServer.TestServer.CreateClient();

            var request = new CreateContactCommand();


            // act

            var response = await httpClient.PutAsJsonAsync("/AddressBook", request);


            // assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

            var validationModel = await response.Content.ReadAsJson<ModelStateError>();

            Assert.Equal(new[]
                {
                    "The EmailAddress field is required.",
                    "The FirstName field is required.",
                    "The Surname field is required."
                },
                validationModel.Errors.SelectMany(s=>s.Value).OrderBy(s=>s).ToList());

        }

        [Fact]
        public async Task Create_Should_execute_handler()
        {
            // arrange
            using var httpClient = _webServer.TestServer.CreateClient();

            var request = new CreateContactCommand
            {
                Surname = "User",
                FirstName = "One",
                EmailAddress = "user.one@test.com",
                DateOfBirth = DateTime.Today.AddYears(-30)
            };

            _webServer.MicroserviceHubMock.Setup(x => x.Send(It.IsAny<CreateContactCommand>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ContactQueryResponse
                {
                    Id = Guid.NewGuid(),
                    Surname = "User",
                    FirstName = "One",
                    EmailAddress = "user.one@test.com",
                    DateOfBirth = DateTime.Today.AddYears(-30)
                });

            // act

            var response = await httpClient.PostAsJsonAsync($"/AddressBook", request);


            // assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var actualModel = await response.Content.ReadAsJson<ContactQueryResponse>();

            Assert.Equal("user.one@test.com", actualModel.EmailAddress);

        }

    }
}
