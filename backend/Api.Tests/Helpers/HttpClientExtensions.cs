using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Api.Tests.Helpers
{
    public static class HttpClientExtensions
    {
        public static async Task<T> ReadAsJson<T>(this HttpContent httpContent)
        {
            var content = await httpContent.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(content, new JsonSerializerSettings());
        }
    }
}