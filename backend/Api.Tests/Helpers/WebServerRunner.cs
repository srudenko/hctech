using System;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Api.Tests.Helpers
{
    public class WebServerRunner<TStartup>: IDisposable where TStartup : class
    {
        public readonly WebApplicationFactory<TStartup> TestServer;

        public readonly IConfigurationRoot Configuration;

        public Mock<IMediator> MicroserviceHubMock { get; } = new();

        public WebServerRunner()
        {
            Configuration = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                .Build();

            TestServer = new WebApplicationFactory<TStartup>()
                .WithWebHostBuilder(builder =>
                {
                    builder.UseEnvironment("UnitTest");

                    builder.ConfigureTestServices(ConfigureDefaultTestServices);
                    builder.ConfigureTestServices(ConfigureTestServices);
                });
        }

        private void ConfigureDefaultTestServices(IServiceCollection services)
        {
            services.AddSingleton(MicroserviceHubMock.Object);
        }

        protected virtual void ConfigureTestServices(IServiceCollection services)
        {
        }

        public virtual void Dispose()
        {
            TestServer?.Dispose();
        }

    }
}