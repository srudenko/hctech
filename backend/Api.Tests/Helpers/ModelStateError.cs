using System.Collections.Generic;

namespace Api.Tests.Helpers
{
    public class ModelStateError
    {
        public Dictionary<string, IList<string>> Errors { get; set; }
    }
}