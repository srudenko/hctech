﻿using DataAccess.DTO;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Context
{
    public class AddressBookContext: DbContext
    {
        public AddressBookContext()
        {
        }

        public AddressBookContext(DbContextOptions<AddressBookContext> options) : base(options)
        {
        }

        public DbSet<ContactDto> Contacts { get; set; }
    }
}