﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contracts.Commands;
using Contracts.Exceptions;
using Contracts.Models;
using Contracts.Queries;
using Contracts.Responses;
using DataAccess.Context;
using DataAccess.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Handler
{
    public class AddressBookHandler :
        IRequestHandler<ListContactsQuery, ListContactsQueryResponse>,
        IRequestHandler<GetContactByEmailQuery, ContactQueryResponse>,
        IRequestHandler<CreateContactCommand, ContactQueryResponse>,
        IRequestHandler<UpdateContactCommand, ContactQueryResponse>
    {
        private readonly AddressBookContext _dbContext;
        private readonly IMediator _mediator;

        public AddressBookHandler(
            AddressBookContext dbContext,
            IMediator mediator)
        {
            _dbContext = dbContext;
            _mediator = mediator;
        }

        public async Task<ListContactsQueryResponse> Handle(ListContactsQuery request, CancellationToken cancellationToken)
        {
            var allContacts = await _dbContext.Contacts.ToListAsync(cancellationToken);
            return new ListContactsQueryResponse(allContacts.Select(MapContact).ToList());
        }

        public async Task<ContactQueryResponse> Handle(GetContactByEmailQuery request, CancellationToken cancellationToken)
        {
            var contactDto = await _dbContext.Contacts.FirstOrDefaultAsync(c => c.EmailAddress == request.EmailAddress, cancellationToken);

            if (contactDto == null)
            {
                return null;
            }

            return new ContactQueryResponse
            {
                Id = contactDto.Id,
                Surname = contactDto.EmailAddress,
                FirstName = contactDto.FirstName,
                EmailAddress = contactDto.EmailAddress,
                DateOfBirth = contactDto.DateOfBirth
            };
        }

        public async Task<ContactQueryResponse> Handle(CreateContactCommand request, CancellationToken cancellationToken)
        {
            var existingContact = await _dbContext.Contacts.FirstOrDefaultAsync(c => c.EmailAddress == request.EmailAddress, cancellationToken: cancellationToken);

            if (existingContact != null)
            {
                throw new UserExistException();
            }

            var contactDto = new ContactDto
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                Surname = request.Surname,
                EmailAddress = request.EmailAddress,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                DateOfBirth = request.DateOfBirth
            };

            await _dbContext.Contacts.AddAsync(contactDto, cancellationToken);

            await _dbContext.SaveChangesAsync(cancellationToken);

            return await _mediator.Send(new GetContactByEmailQuery
            {
                EmailAddress = request.EmailAddress
            }, cancellationToken);
        }

        public async Task<ContactQueryResponse> Handle(UpdateContactCommand request, CancellationToken cancellationToken)
        {
            var existingContact = await _dbContext.Contacts.FirstOrDefaultAsync(c => c.EmailAddress == request.EmailAddress, cancellationToken: cancellationToken);

            if (existingContact == null)
            {
                throw new NotFoundException();
            }

            existingContact.FirstName = request.FirstName;
            existingContact.Surname = request.Surname;
            existingContact.DateOfBirth = request.DateOfBirth;
            existingContact.UpdatedAt = DateTime.UtcNow;

            _dbContext.Contacts.Update(existingContact);

            await _dbContext.SaveChangesAsync(cancellationToken);

            return await _mediator.Send(new GetContactByEmailQuery
            {
                EmailAddress = request.EmailAddress
            }, cancellationToken);

        }

        private static Contact MapContact(ContactDto dto)
        {
            return new Contact
            {
                Id = dto.Id,
                Surname = dto.Surname,
                EmailAddress = dto.EmailAddress,
                FirstName = dto.FirstName,
                DateOfBirth = dto.DateOfBirth
            };
        }

    }
}