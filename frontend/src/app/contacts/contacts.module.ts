import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsComponent } from './list/contacts.component';
import { ContactsRoutingModule } from "./contacts-routung.module";
import { SharedModule } from "../shared.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactService } from "./contacts.service";
import { ContactEditComponent } from './edit/contact-edit.component';

@NgModule({
    declarations: [
        ContactsComponent,
        ContactEditComponent
    ],
    imports: [
        CommonModule,
        ContactsRoutingModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [ContactService]
})
export class ContactsModule {
}
