import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { ContactItem } from "../store/models/contact-item.model";
import { Store } from "@ngrx/store";
import { AppState } from "../store/models/app-state.model";
import { MatDialog } from "@angular/material/dialog";
import {
    LoadContactsAction
} from "../store/actions/contact.actions";
import { Router } from "@angular/router";

@Component({
    selector: 'app-contacts',
    templateUrl: './contacts.component.html',
    styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
    contactItems$: Observable<Array<ContactItem>> | any;

    constructor(private store: Store<AppState>, public dialog: MatDialog, private router: Router) {
    }

    public ngOnInit(): void {
        this.contactItems$ = this.store.select((store: any) => store.contact.list);
        this.store.dispatch(new LoadContactsAction());

    }

    public onAddNewContact() {
        this.router.navigate(['contacts/new']);
    }

    public onEditContact(contact: ContactItem) {

        console.log(`Opening a new contact: ${contact.emailAddress}`);
        this.router.navigate(['contacts/edit', {emailAddress: contact.emailAddress}]);
    }

}
