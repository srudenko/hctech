import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactsComponent } from "./list/contacts.component";
import { ContactEditComponent } from "./edit/contact-edit.component";


const routes: Routes = [
    {
        path: '',
        component: ContactsComponent
    },
    {
        path: 'new',
        component: ContactEditComponent
    }
    ,
    {
        path: 'edit',
        component: ContactEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ContactsRoutingModule {
}
