import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ValidationService } from "../../services/validationService";
import { AppState } from "../store/models/app-state.model";
import { Store } from "@ngrx/store";
import { AddContactAction, ChangeContactAction, LoadContactsAction } from "../store/actions/contact.actions";
import { ContactItem } from "../store/models/contact-item.model";
import { DateTimeService } from "../../services/dateTimeService";

@Component({
    selector: 'app-contact-edit',
    templateUrl: './contact-edit.component.html',
    styleUrls: ['./contact-edit.component.scss']
})
export class ContactEditComponent implements OnInit {
    private readonly emailAddress?: string = undefined;
    public userForm: FormGroup;
    public userExists: boolean = false;

    constructor(private route: ActivatedRoute,
                private formBuilder: FormBuilder,
                private store: Store<AppState>,
                private router: Router) {

        // NB: snapshot used for simplicity.
        // A better approach is to use this.route.queryParams.subscribe()
        this.emailAddress = this.route.snapshot.params.emailAddress;

        this.userForm = this.formBuilder.group({
            firstName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]],
            surname: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]],
            emailAddress: ['', [Validators.required, ValidationService.emailValidator]],
            dateOfBirth: ['', [Validators.required, ValidationService.dateOfBirthValidator, ValidationService.dateFormat]],
        });
    }

    ngOnInit(): void {

        // NB: this can be retrieved by email
        this.store.select((store: any) => store.contact.list)
            .subscribe((response: Array<ContactItem>) => {

                const foundContact = response.filter(c => c.emailAddress == this.emailAddress);

                if (foundContact?.length > 0) {
                    this.userExists = true;

                    // convert date to UTC and get Date part
                    const utcDate = DateTimeService.getUtcDateFromISO861(foundContact[0].dateOfBirth);
                    this.userForm.patchValue({
                        ...foundContact[0],
                        dateOfBirth: utcDate
                    });
                }

            });
        this.store.dispatch(new LoadContactsAction());

    }

    saveUser() {
        if (this.userForm.dirty && !this.userForm.valid) {
            return;
        }


        // create or update user
        if (this.userExists) {
            this.store.dispatch(new ChangeContactAction({
                ...this.userForm.value
            }));
        } else {
            this.store.dispatch(new AddContactAction({
                ...this.userForm.value
            }));
        }

        this.router.navigate(['contacts']);
    }

    openAddressBook() {
        this.router.navigate(['contacts']);
    }

}
