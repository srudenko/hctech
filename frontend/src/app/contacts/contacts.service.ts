import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContactItem } from "./store/models/contact-item.model";
import { environment } from "../../environments/environment";
import { ListContactsQueryResponse } from "./store/models/contacts-response.model.tf";


@Injectable({
    providedIn: 'root'
})
export class ContactService {

    private ADDRESS_BOOK_URL = environment.API_URL + '/addressBook'

    constructor(private http: HttpClient) {
    }

    getContactItems() {
        return this.http.get<ListContactsQueryResponse>(`${this.ADDRESS_BOOK_URL}/all`)
            .pipe()
    }

    addContactItem(contactItem: ContactItem) {
        return this.http.post(this.ADDRESS_BOOK_URL, contactItem)
            .pipe()
    }

    changeContactItem(contactItem: ContactItem) {
        return this.http.put(this.ADDRESS_BOOK_URL, contactItem)
            .pipe()
    }

    deleteContactItem(id: string) {
        return this.http.delete(`${this.ADDRESS_BOOK_URL}/${id}`)
            .pipe()
    }
}
