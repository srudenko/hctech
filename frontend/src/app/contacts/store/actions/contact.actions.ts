import { Action } from '@ngrx/store';
import { ContactItem } from '../models/contact-item.model';

export enum ContactActionTypes {
    LOAD_CONTACTS = 'Load Contacts',
    LOAD_CONTACTS_SUCCESS = 'Load Contacts Success',
    LOAD_CONTACTS_FAILURE = 'Load Contacts Failure',
    ADD_CONTACT = 'Add Contact',
    ADD_CONTACT_SUCCESS = 'Add Contact Success',
    ADD_CONTACT_FAILURE = 'Add Contact Failure',
    CHANGE_CONTACT = 'Change Contact',
    CHANGE_CONTACT_SUCCESS = 'Change Contact Success',
    CHANGE_CONTACT_FAILURE = 'Change Contact Failure',
}

export class LoadContactsAction implements Action {
    readonly type = ContactActionTypes.LOAD_CONTACTS
}

export class LoadContactsSuccessAction implements Action {
    readonly type = ContactActionTypes.LOAD_CONTACTS_SUCCESS

    constructor(public payload: Array<ContactItem>) {
    }

}

export class LoadContactsFailureAction implements Action {
    readonly type = ContactActionTypes.LOAD_CONTACTS_FAILURE

    constructor(public payload: string) {
    }
}

export class AddContactAction implements Action {
    readonly type = ContactActionTypes.ADD_CONTACT

    constructor(public payload: ContactItem) {
    }
}

export class AddContactSuccessAction implements Action {
    readonly type = ContactActionTypes.ADD_CONTACT_SUCCESS

    constructor(public payload: ContactItem) {
    }
}

export class AddContactFailureAction implements Action {
    readonly type = ContactActionTypes.ADD_CONTACT_FAILURE

    constructor(public payload: Error) {
    }
}

export class ChangeContactAction implements Action {
    readonly type = ContactActionTypes.CHANGE_CONTACT

    constructor(public payload: ContactItem) {
    }
}

export class ChangeContactSuccessAction implements Action {
    readonly type = ContactActionTypes.CHANGE_CONTACT_SUCCESS

    constructor(public payload: ContactItem) {
    }
}

export class ChangeContactFailureAction implements Action {
    readonly type = ContactActionTypes.CHANGE_CONTACT_FAILURE

    constructor(public payload: Error) {
    }
}

export type ContactAction = AddContactAction |
    AddContactSuccessAction |
    AddContactFailureAction |
    ChangeContactAction |
    ChangeContactSuccessAction |
    ChangeContactFailureAction |
    LoadContactsAction |
    LoadContactsFailureAction |
    LoadContactsSuccessAction
