﻿import { ContactItem } from "./contact-item.model";

// NB: Ideally, this should be generated based on OpenApi.json (Swagger.json)
// see ListContactsQueryResponse in https://localhost:5001/swagger/v1/swagger.json
export interface ListContactsQueryResponse {
    contacts: Array<ContactItem>;
}