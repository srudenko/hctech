import { ContactState } from '../reducers/contact.reducer';

export interface AppState {
    readonly contact: ContactState
}
