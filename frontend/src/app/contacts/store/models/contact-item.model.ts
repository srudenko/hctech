﻿// NB: Ideally, this should be generated based on OpenApi.json (Swagger.json)
// see Contact in https://localhost:5001/swagger/v1/swagger.json
export interface ContactItem {
    id?: string;
    firstName: string;
    surname: string;
    emailAddress: string;
    dateOfBirth: string;
}
