import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap, catchError } from 'rxjs/operators';

import {
    LoadContactsAction,
    ContactActionTypes,
    LoadContactsSuccessAction,
    LoadContactsFailureAction,
    AddContactAction,
    AddContactSuccessAction,
    AddContactFailureAction,
    ChangeContactFailureAction, ChangeContactSuccessAction, ChangeContactAction
} from '../actions/contact.actions'
import { of } from 'rxjs';
import { ContactService } from "../../contacts.service";
import { Store } from "@ngrx/store";
import { AppState } from "../models/app-state.model";

@Injectable()
export class ContactEffects {

    @Effect() loadContact$ = this.actions$
        .pipe(
            ofType<LoadContactsAction>(ContactActionTypes.LOAD_CONTACTS),
            mergeMap(
                () => this.contactService.getContactItems()
                    .pipe(
                        map(data => {
                            return new LoadContactsSuccessAction(data.contacts)
                        }),
                        catchError(error => of(new LoadContactsFailureAction(error)))
                    )
            ),
        )

    @Effect() addContactItem$ = this.actions$
        .pipe(
            ofType<AddContactAction>(ContactActionTypes.ADD_CONTACT),
            mergeMap(
                (data) => this.contactService.addContactItem(data.payload)
                    .pipe(
                        map(() => {
                            new AddContactSuccessAction(data.payload);
                            this.store.dispatch(new LoadContactsAction());
                        }),
                        catchError(error => of(new AddContactFailureAction(error)))
                    )
            )
        )

    @Effect() changeContactItem$ = this.actions$
        .pipe(
            ofType<ChangeContactAction>(ContactActionTypes.CHANGE_CONTACT),
            mergeMap(
                (data) => this.contactService.changeContactItem(data.payload as any)
                    .pipe(
                        map(() => {
                                new ChangeContactSuccessAction(data.payload)
                                this.store.dispatch(new LoadContactsAction());
                            }
                        ),
                        catchError(error => of(new ChangeContactFailureAction(error)))
                    )
            )
        )

    constructor(
        private actions$: Actions,
        private contactService: ContactService,
        private store: Store<AppState>
    ) {
    }
}
