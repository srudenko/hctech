import { ContactActionTypes, ContactAction } from '../actions/contact.actions';
import { ContactItem } from '../models/contact-item.model';

export interface ContactState {
    list: ContactItem[],
    loading: boolean,
    error: Error;
}


const initialState: ContactState = {
    list: [],
    loading: false,
    error: undefined as any
};

export function ContactReducer(state: ContactState = initialState, action: ContactAction | any) {
    switch (action.type) {
        case ContactActionTypes.LOAD_CONTACTS:
            return {
                ...state,
                loading: true
            }
        case ContactActionTypes.LOAD_CONTACTS_SUCCESS:
            return {
                ...state,
                list: action.payload,
                loading: false
            }

        case ContactActionTypes.LOAD_CONTACTS_FAILURE:
            return {
                ...state,
                error: action.payload
            }

        case ContactActionTypes.ADD_CONTACT:
            return {
                ...state,
                loading: true
            }
        case ContactActionTypes.ADD_CONTACT_SUCCESS:
            return {
                ...state,
                list: [...state.list, action.payload],
                loading: false
            };
        case ContactActionTypes.ADD_CONTACT_FAILURE:
            return {
                ...state,
                error: action.payload,
                loading: false
            };
        case ContactActionTypes.CHANGE_CONTACT:
            return {
                ...state,
                loading: true
            }
        case ContactActionTypes.CHANGE_CONTACT_SUCCESS:
            const newList = state.list.map(el => {
                return action.payload.id === el.id ? action.payload : el;
            })
            return {
                ...state,
                list: newList,
                loading: false
            };
        case ContactActionTypes.CHANGE_CONTACT_FAILURE:
            return {
                ...state,
                error: action.payload,
                loading: false
            };

        default:
            return state;
    }
}
