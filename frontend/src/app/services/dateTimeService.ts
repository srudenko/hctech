﻿export class DateTimeService {

    public static getUtcDateFromISO861(dateTime: string) {
        let date = new Date(dateTime);
        const offset = date.getTimezoneOffset()
        date = new Date(date.getTime() - (offset * 60 * 1000))

        return date.toISOString().split('T')[0];
    }

}