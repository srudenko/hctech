﻿export class ValidationService {
    public static emailValidator(control: { value: string }) {
        // RFC 2822 compliant regex
        if (
            control.value.match(
                /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
            )
        ) {
            return null;
        } else {
            return {invalidEmailAddress: true};
        }
    }

    public static dateOfBirthValidator(control: { value: string }) {
        if (
            new Date(control.value) < new Date()
        ) {
            return null;
        } else {
            return {invalidDateOfBirth: true};
        }
    }

    public static dateFormat(control: { value: string }) {
        if (
            control.value.match(
                /^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/
            )
        ) {
            return null;
        } else {
            return {dateFormat: true};
        }
    }
}