import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "./shared.module";
import { RouterModule } from "@angular/router";
import { environment } from "../environments/environment";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { HttpClientModule } from "@angular/common/http";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { ContactEffects } from "./contacts/store/effects/contact.effects";
import { ContactReducer } from "./contacts/store/reducers/contact.reducer";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        RouterModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
        StoreModule.forRoot({
            contact: ContactReducer,
        }),
        EffectsModule.forRoot([ContactEffects])
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
