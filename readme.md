# Local setup

## Backend

1) Point connection string in `appsettings.Development` to SqlServer

in `backend\Api` folder

```
  "ConnectionStrings": {
    "Contacts": "server=.;database=Contacts;trusted_connection=true;MultipleActiveResultSets=True"
  }

```

2) Run EntityFramework migration to create `dbo.Contacts` table

in `backend\Api` folder

```
dotnet ef database update
```

3) Run Api app

in `backend\Api` folder

```
dotnet run
```


## Swagger is available on

https://localhost:5001/swagger/index.html

## Run angular app

in `frontend` folder

```
npm install
npm run start

```
or

```
yarn
yarn start
```

## Angular app is available on URL

http://localhost:4200/


## Run unit tests in Backend

in `backend\Api.Tests`


```
dotnet tests
```